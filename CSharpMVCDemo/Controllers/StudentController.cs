﻿using CSharpMVCDemo.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics.SymbolStore;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace CSharpMVCDemo.Controllers
{
    public class StudentController : Controller
    {
        static IList<Student> studentList = new List<Student>{
                new Student() { StudentId = 1, StudentName = "John", Age = 18 } ,
                new Student() { StudentId = 2, StudentName = "Steve",  Age = 21 } ,
                new Student() { StudentId = 3, StudentName = "Bill",  Age = 25 } ,
                new Student() { StudentId = 4, StudentName = "Ram" , Age = 20 } ,
                new Student() { StudentId = 5, StudentName = "Ron" , Age = 31 } ,
                new Student() { StudentId = 6, StudentName = "Chris" , Age = 17 } ,
                new Student() { StudentId = 7, StudentName = "Rob" , Age = 19 }
            };
        // GET: Student
        public ActionResult DisplayStudent()
        {

            return View(studentList.OrderBy(s => s.StudentId).ToList());

        }
        public ActionResult Index()
        {
            TempData["name"] = "Bang";
            ViewBag.TotalStudents = studentList.Count();

            ViewData["students"] = studentList;

            return View();
        }
        //Edit
        public ActionResult Edit(int Id)
        {
            //here, get the student from the database in the real application

            //getting a student from collection for demo purpose
            var std = studentList.Where(s => s.StudentId == Id).FirstOrDefault();

            return View(std);
        }
        [HttpPost]
        public ActionResult Edit(Student std)
        {
            if (ModelState.IsValid)
            { //checking model state

                //update student to db
                var student = studentList.Where(s => s.StudentId == std.StudentId).FirstOrDefault();
                studentList.Remove(student);
                studentList.Add(std);
                return RedirectToAction("DisplayStudent");
            }
            return View(std);

        }

        //Delete
        public ActionResult Delete(int Id)
        {
            var std = studentList.Where(s => s.StudentId == Id).FirstOrDefault();

            return View(std);
        }


        [HttpPost, ActionName("Delete")]
        public ActionResult Delete(string Id)
        {
            int CurrentId = Int32.Parse(Id);
            var std = studentList.Where(s => s.StudentId == CurrentId).FirstOrDefault();
            studentList.Remove(std);
            return RedirectToAction("DisplayStudent");
        }

        //Create 
        public ActionResult Create()
        {
            return View(); 
        }
        [HttpPost]
        public ActionResult Create(Student std)
        {
            if (ModelState.IsValid)
            { //checking model state
                var st = studentList.OrderByDescending(s => s.StudentId).FirstOrDefault(); 
                //update student to db
                studentList.Add(std);
                std.StudentId = st.StudentId +1;
                return RedirectToAction("DisplayStudent");
            }
           
            return View(std);

        }

        public ActionResult Detail(int Id)
        {
            //getting a student from collection for demo purpose
            var std = studentList.Where(s => s.StudentId == Id).FirstOrDefault();

            return View(std);
        }
    }
}