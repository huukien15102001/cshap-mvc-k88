﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CSharpMVCDemo.Controllers
{
    public class ErrorHandlingController : Controller
    {
        // GET: ErrorHandling
        public ActionResult Index()
        {
            return View();
        }
        [HandleError]
        [HandleError(ExceptionType = typeof(DivideByZeroException), View = "Error1")]
        [HandleError(ExceptionType = typeof(ArgumentOutOfRangeException), View = "Error2")]
        public ActionResult Index1()
        {
            int a = 1;
            int b = 0;
            int c = 0;
            c = a / b; //it would cause exception.
            return View();
        }


        [HandleError(ExceptionType = typeof(DivideByZeroException), View = "Error1")]
        [HandleError(ExceptionType = typeof(ArgumentOutOfRangeException), View = "Error2")]
        [HandleError] // this will work, go to the default: view = "Error";
        public ActionResult Index2()
        {
            int a = 1;
            int b = 0;
            int c = 0;
            c = a / b; //it would cause exception.
            return View();
        }
    }
}