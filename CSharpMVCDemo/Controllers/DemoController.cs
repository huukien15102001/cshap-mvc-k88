﻿using CSharpMVCDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CSharpMVCDemo.Controllers
{
    public class DemoController : Controller
    {
        // GET: Demo
        public ActionResult Index()
        {
            return View(new Demo());
        }

        [HttpPost]

        public ActionResult Index(Demo c, string calc)
        {
            if(calc == "add")
            {
                c.tot = c.no1 + c.no2;
            }
            else if(calc == "min")
            {
                c.tot = c.no1 - c.no2;
            }
            else if (calc == "sub")
            {
                c.tot = c.no1 * c.no2;
            }
            else 
            {
                c.tot = c.no1 / c.no2;
            }
            return View(c);
        }
        
    }
}