﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CSharpMVCDemo.Models
{
    public class Student
    {
        [Required]
        [DataType(DataType.Text)]
        [DisplayName("StudentId")]
        public int StudentId { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [DisplayName("StudentName")]
        public string StudentName { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        [DisplayName("Age")]
        public int Age { get; set; }
    }
}